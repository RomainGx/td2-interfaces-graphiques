package fr.isima.td_interfacesgraphiques;

import android.app.Activity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ViewFlipper;

public class MainActivity extends Activity {
    private ViewFlipper flipper;

    private SparseArray<String> layoutNames;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.flipper);

        layoutNames = new SparseArray<>();
        layoutNames.put(R.id._0_cours_linear_layout_1, "3 views wrap");
        layoutNames.put(R.id._0_cours_linear_layout_2, "2 views wrap + 1 place dispo");
        layoutNames.put(R.id._0_cours_linear_layout_3, "Partage = 50% / 50%");
        layoutNames.put(R.id._0_cours_linear_layout_4, "Partage = 75% / 25%");
        layoutNames.put(R.id._1_linear_layout, "UI connexion (linear)");
        layoutNames.put(R.id._1_relative_layout, "UI connexion (relative)");
        layoutNames.put(R.id._1_table_layout, "UI connexion (table)");
        layoutNames.put(R.id._2_relative_layout, "Btn en bas");
        layoutNames.put(R.id._3_relative_layout, "Btn en bas wrap width");
        layoutNames.put(R.id._4_relative_layout, "Btn centre / page");
        layoutNames.put(R.id._5_relative_layout, "Btn centre / place dispo");

        flipper = findViewById(R.id.viewFlipper);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.flipper_menu, menu);
        menu.findItem(R.id.prevLayout).setVisible(false);
        getActionBar().setTitle(layoutNames.get(flipper.getCurrentView().getId()));
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.findItem(R.id.prevLayout).setVisible(flipper.getDisplayedChild() != 0);
        menu.findItem(R.id.nextLayout).setVisible(flipper.getDisplayedChild() < flipper.getChildCount() - 1);
        getActionBar().setTitle(layoutNames.get(flipper.getCurrentView().getId()));
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.prevLayout:
                flipper.showPrevious();
                invalidateOptionsMenu();
                return true;

            case R.id.nextLayout:
                flipper.showNext();
                invalidateOptionsMenu();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
